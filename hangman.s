.global main
@ author Robyn Greer
@ COM1031 Coursework - Hangman


main:
@ number of guesses go here
@ start with 6
mov r4, #54
@ r6 stores array index for correct inputs to go
mov r6, #0
@ r8 stores array index for incorrect inputs to go to
mov r8, #0
@ stores number for checking loops in guesschar function
mov r10, #0
@call function that prints welcome message
bl printwelcome

@ call function to get word to be guessed
bl getword

@ main game loop
mainloop:
bl printnewline

@ print dashes representing hidden word
bl printword
bl printnewline

@ print number of remaining guesses
bl printguesses
bl printnewline

@ print correctly guessed letters
bl printcorrect
bl printnewline

@ print incorrectly guessed letters
bl printmisses
bl printnewline

@ print gallows
bl printhangman
bl printnewline

@ skip here if error encountered
skip:
@ if number of guesses = 0 end game
cmp r4, #48
beq lose

@ check if user has entered all correct characters
@ if yes branch to win and end game
bl checkwin
cmp r5, #1
beq win

@ allow user to guess a character
bl guesschar

@ check for error
cmp r9, #1
beq skip

@ loop again to allow multiple guesses
b mainloop

@ print loss message and reveal hidden word
lose:
@ print message
mov r7, #4
mov r0, #1
mov r2, #28
ldr r1, =losemsg
svc 0
@ print the word
ldr r0, =message
bl puts


@ branch to end of program
b end

@ prints win message if player wins
win:
@ print message
mov r7, #4
mov r0, #1
mov r2, #7
ldr r1, =winmsg
svc 0

b end

@ stores random word in =message
getword:
push {r0-r3, lr}
@ get random number
mov r0, #0
bl time
bl srand
bl rand
and r0, r0, #9

@ store rand num in r1
mov r1, r0

@ store word in message according to rand num
cmp r1, #0
beq str1
cmp r1, #1
beq str2
cmp r1, #2
beq str3
cmp r1, #3
beq str4
cmp r1, #4
beq str5
cmp r1, #5
beq str6
cmp r1, #6
beq str7
cmp r1, #7
beq str8
cmp r1, #8
beq str9
cmp r1, #9
beq str10

str1:
ldr r2, =w1
bl storeword
b endgetword

str2:
ldr r2, =w2
bl storeword
b endgetword

str3:
ldr r2, =w3
bl storeword
b endgetword


str4:
ldr r2, =w4
bl storeword
b endgetword

str5:
ldr r2, =w5
bl storeword
b endgetword

str6:
ldr r2, =w6
bl storeword
b endgetword

str7:
ldr r2, =w7
bl storeword
b endgetword

str8:
ldr r2, =w8
bl storeword
b endgetword

str9:
ldr r2, =w9
bl storeword
b endgetword

str10:
ldr r2, =w5
bl storeword
b endgetword

endgetword:
pop {r0-r3, lr}
bx lr


@ stores a word in message
@ argument r2, address of word to store in message
storeword:
push {r0, r1, r3, lr}
@ counter
mov r3, #0
@ address to store chars in
ldr r1, =message
swloop:
ldrb r0, [r2], #1
@ if char is 0 end
cmp r0, #0
beq endstoreword
@ else store it in message
str r0, [r1, r3]
@ increment counter
add r3, r3, #1
@ loop
b swloop
endstoreword:
pop {r0, r1, r3, lr}
bx lr


@ checks if there are any characters the user hasn't guessed yet
@ returns r5, boolean determining whether or not user is missing a character
checkwin:
push {r0-r3, lr}
@ get the word to be guessed
ldr r0, =message
@ loop through and check if each letter is in allinput (list of correct guesses)
@ by default return true
mov r5, #1
winloop:
@ load character of the word and advance
ldrb r1, [r0], #1
@ if it's 0 endloop
cmp r1, #0
beq endcheckwin

@ if its a comma don't check just skip to next char
cmp r1, #44
beq winloop

@ if its a space don't check just loop to next char
cmp r1, #32
beq winloop

@ check if it's in allinput
@ if not return false
@ user hasn't guessed all characters and therefore hasn't won yet

bl charguessed

cmp r3, #0
beq false
@ loop for next character
b winloop

false:
@ if any character from the word is missing from input list, return false
@ the user hasn't yet won
mov r5, #0
b endcheckwin

endcheckwin:
pop {r0-r3, lr}
bx lr

@ checks if a character has been guessed correctly
@ argument r1, the character
@ return r3, boolean determining whether or not user guessed the character
charguessed:
push {r0, r2, lr}
ldr r0, =allinput
@ returns false by default
mov r3, #0
cgloop:
@ load character of allinput and advance
ldrb r2, [r0], #1
@ if it's 0 end loop
cmp r2, #0
beq endcg
@ check if it's equal to argument character
cmp r2, r1
beq true
@ if yes return true
@ loop again
b cgloop

true:
mov r3, #1
b endcg

endcg:
pop {r0, r2, lr}
bx lr



@ gets length of word
@ argument r0, address of word to get length of
@ return r2, length of word
getlen:
push {r1, lr}
mov r2, #0
@ loops through word and increments counter for each letter 
lenloop:
ldrb r1, [r0], #1
cmp r1, #0
addne r2, r2, #1
bne lenloop
pop {r1, lr}
bx lr

@ print user's previous correct guesses
@ since printing it with the dashes isn't working
printcorrect:
push {r0-r3, lr}
@ print Correct:
mov r7, #4
mov r0, #1
mov r2, #9
ldr r1, =correctmsg
svc 0

@ print each character in allinput
ldr r0, =allinput
correctloop:
ldrb r3, [r0], #1
cmp r3, #0
beq endprintcorrect
cmp r3, #44
beq correctloop
bl printchar
b correctloop

endprintcorrect:
pop {r0-r3, lr}
bx lr

@ print user's previously guessed letters
printmisses:
push {r0-r3, lr}
@ print Misses:
mov r7, #4
mov r0, #1
mov r2, #8
ldr r1, =missesmsg
svc 0

@ print each character in misses
ldr r0, =misses
missesloop:
ldrb r3, [r0], #1
cmp r3, #0
beq endprintmisses
cmp r3, #44
beq missesloop
bl printchar
b missesloop

endprintmisses:
pop {r0-r3, lr}
bx lr

@ print new line
printnewline:
push {r0-r2, lr}
mov r7, #4
mov r0, #1
mov r2, #2
ldr r1, =newline
svc 0
pop {r0-r2, lr}
bx lr

@ prints the gallows according to number of remaining guesses
@ argument r4, the number of remaining guesses
printhangman:
push {r0-r2, lr}
cmp r4, #54
bleq printg6hangman
cmp r4, #53
bleq printg5hangman
cmp r4, #52
bleq printg4hangman
cmp r4, #51
bleq printg3hangman
cmp r4, #50
bleq printg2hangman
cmp r4, #49
bleq printg1hangman
cmp r4, #48
bleq printg0hangman
pop {r0-r2, lr}
bx lr

@ each of these prints a different version of the gallows
printg6hangman:
push {r0-r2, lr}
mov r7, #4
mov r0, #1
mov r2, #100
ldr r1, =g6hangman
svc 0
pop {r0-r2, lr}
bx lr



printg5hangman:
push {r0-r2, lr}
mov r7, #4
mov r0, #1
mov r2, #100
ldr r1, =g5hangman
svc 0
pop {r0-r2, lr}
bx lr


printg4hangman:
push {r0-r2, lr}
mov r7, #4
mov r0, #1
mov r2, #100
ldr r1, =g4hangman
svc 0
pop {r0-r2, lr}
bx lr


printg3hangman:
push {r0-r2, lr}
mov r7, #4
mov r0, #1
mov r2, #100
ldr r1, =g3hangman
svc 0
pop {r0-r2, lr}
bx lr


printg2hangman:
push {r0-r2, lr}
mov r7, #4
mov r0, #1
mov r2, #100
ldr r1, =g2hangman
svc 0
pop {r0-r2, lr}
bx lr


printg1hangman:
push {r0-r2, lr}
mov r7, #4
mov r0, #1
mov r2, #100
ldr r1, =g1hangman
svc 0
pop {r0-r2, lr}
bx lr


printg0hangman:
push {r0-r2, lr}
mov r7, #4
mov r0, #1
mov r2, #100
ldr r1, =g0hangman
svc 0
pop {r0-r2, lr}
bx lr


@ prints dashes representing hidden word
printword:
push {r0-r2, lr}

@ print Word:
mov r7, #4
mov r0, #1
mov r2, #6
ldr r1, =word
svc 0

ldr r0, =message
@ loop through hidden word and print dash for each letter
pwloop:
ldrb r1, [r0], #1
cmp r1, #0
beq endprintword

bl printdash

b pwloop

endprintword:
pop {r0-r2, lr}
bx lr

@ prints a character
@ argument r3, the byte from the list of input characters
printchar:
push {r0-r2, lr}
ldr r1, =chartoprint
str r3, [r1]
mov r7, #4
mov r0, #1
mov r2, #1
ldr r1, =chartoprint
svc 0
pop {r0-r2, lr}
bx lr

@ prints welcome message
printwelcome:
push {r0-r3, lr}
mov r7, #4
mov r0, #1
mov r2, #64
ldr r1, =welcome
svc 0
pop {r0-r3, lr}
bx lr

@ prints an underscore
printdash:
push {r0-r2, lr}
mov r7, #4
mov r0, #1
mov r2, #1
ldr r1, =dash
svc 0
pop {r0-r2, lr}
bx lr

@ prints number of guesses with message
printguesses:
push {r0-r3, lr}

@ updates number of guesses in =guesses
ldr r1, =guesses
str r4, [r1]

@ prints Guesses:
mov r7, #4
mov r0, #1
mov r2, #10
ldr r1, =showguesses
svc 0

@ prints number of guesses remaining
mov r0, #1
ldr r1, =guesses
mov r2, #1
mov r7, #4
svc 0

pop {r0-r3, lr}
bx lr

@ checks if an incorrect character has already been entered
@ argument r3 the character to check
checkwrongentered:
push {r0-r2, lr}
ldr r0, =misses
cweloop:
@ load byte of misses array and advance
ldrb r1, [r0], #1

@ if it's 0 end loop
cmp r1, #0
beq endcwe

@ if it's equal to the input branch to repeat
cmp r1, r3
beq repeat

@ else loop back round for next char of misses array
b cweloop

endcwe:
pop {r0-r2, lr}
bx lr

@ checks if input character has been entered already
@ argument r3 the input character
checkcorrectentered:
push {r0-r2, lr}
ldr r0, =allinput
cceloop:
@ load byte of allinput array and advance
ldrb r1, [r0], #1

@ if it's 0 end loop
cmp r1, #0
beq endcce

@ if it's equal to input branch to repeat
cmp r1, r3
beq repeat

@ else loop back round fpr next char of allinput array
b cceloop

endcce:
pop {r0-r2, lr}
bx lr

@ prompts user to enter a character
@ once they have, check it's valid and correct
@ then print appropiate feedback
guesschar:
push {r0-r2, lr}

@ prompt user for input
mov r7, #4
mov r0, #1
mov r2, #22
ldr r1, =prompt
svc 0

@ take input of 1 character
mov r7, #3
mov r0, #0
mov r2, #1
ldr r1, =input
svc 0
ldr r0, =message
ldrb r3, [r1]


@ reset error boolean
mov r9, #0

@ check if input character 0
@ if yes end program
cmp r3, #48
beq end

@ check if it's a line feed
@ ie, an error
cmp r3, #10
beq error

@ check if it's an invalid character
cmp r3, #64
ble invalid
cmp r3, #123
bge invalid
cmp r3, #91
beq invalid
cmp r3, #92
beq invalid
cmp r3, #93
beq invalid
cmp r3, #94
beq invalid
cmp r3, #95
beq invalid
cmp r3, #96
beq invalid

@ check whether it's a capital
@ if it is skip next section
@ if it's not branch there to get turned into a capital
cmp r3, #90
bgt caps
ble repeatcheck

@ if input isn't a capital, change it so it is
caps:
@ change input at r3 to capital
sub r3, r3, #32
@ store that in =input
strb r3, [r1]

repeatcheck:
@ reset r10, error boolean
mov r10, #0

@ call functions to check if input has already been entered
bl checkwrongentered
bl checkcorrectentered

@ check if input character is in the word to be guessed
guessloop:
@ if r10 is 1 end
@ (repeat skips here sometimes)

cmp r10, #1
beq endguesschar

@ load character of hidden word and advacne
ldrb r2, [r0], #1

@ if end of word is reached user guessed wrong
@ so branch to _wrong
cmp r2, #0
beq _wrong

@ if input character matches character of hidden word user guessed correctly
@ so branch to _correct
cmp r2, r3
beq _correct

@ loop again to check all characters
b guessloop

@ if the character wasn't in the word branch here and print wrong
@ first, decrement guesses and add input character to list of wrong guesses
_wrong:
@ add incorrect letter to list of incorrect letters
ldr r1, =misses
ldr r2, =input
ldr r2, [r2]
str r2, [r1, r8]

@ update index for misses array
add r8, r8, #1

@ decrement guesses
sub r4, r4, #1

@ print wrong message
mov r7, #4
mov r0, #1
mov r2, #5
ldr r1, =wrong
svc 0
b endguesschar


@ if the character was in the word branch here and print correct
_correct:
@ store input in list of correct input
ldr r1, =allinput
ldr r2, =input
ldr r2, [r2]
str r2, [r1, r6]

@ increment array index for correct input
add r6, r6, #1

@ print correct message
mov r7, #4
mov r0, #1
mov r2, #7
ldr r1, =correct
svc 0
b endguesschar


invalid:
@ print invalid input message
mov r7, #4
mov r0, #1
mov r2, #13
ldr r1, =invalidmsg
svc 0
b endguesschar

repeat:
@ set r10 to true to avoid doing guessloop again
mov r10, #1
@ print message saying character has already been entered
mov r7, #4
mov r0, #1
mov r2, #15
ldr r1, =repeatmsg
svc 0
b endguesschar

error:
@ r9 holds boolean for whether error happened or not
mov r9, #1
@ print error message
mov r7, #4
mov r0, #1
mov r2, #5
ldr r1, =errormsg
svc 0
@ print a new line
bl printnewline
b endguesschar

endguesschar:
pop {r0-r2, lr}
bx lr

@ print end message and exit program
end:
bl printnewline
mov r7, #4
mov r0, #1
mov r2, #14
ldr r1, =endmsg
svc 0
bl printnewline

mov r7, #1
svc 0

.data
@ list of words to guess
w1: .asciz "TESTS"
w2: .asciz "CHALLENGE"
w3: .asciz "UNIVERSITY"
w4: .asciz "STUDENTS"
w5: .asciz "BALANCE"
w6: .asciz "FEEDBACK"
w7: .asciz "BINARY"
w8: .asciz "INTELLIGENCE"
w9: .asciz "CARTOGRAPHERS"
w10: .asciz "CHARACTERISTICALLY"
.align 4
@ printed before printing correctly guessed letters
correctmsg: .asciz "Correct: "
.align 4
@ printed when player enters a character again
repeatmsg: .asciz "ALREADY GUESSED"
.align 4
@ printed when error happens
errormsg: .asciz "ERROR"
.align 4
@ printed when player guesses word
winmsg: .asciz "You win"
.align 4
@ printed when player doesn't guess word
losemsg: .asciz "You lost, the word was: "
@ printed alog with the number of guesses left
.align 4
showguesses: .asciz "Guesses: "
.align 4
@ stores the current number of remaining guesses
guesses: .asciz " "
.align 4
@ prompt for user to enter a guess
prompt: .asciz "Enter a character: "
.align 4
@ stores user input
input: .asciz " "
.align 4
@ stores incorrectly guessed letters
misses: .asciz " , , , , , ,"
.align 4
@ stores word for player to guess
message: .asciz ""
.align 4
@ printed when player guesses a letter wrong
wrong: .asciz "WRONG"
.align 4
@ printed when player guesses a letter correctly
correct: .asciz "CORRECT"
.align 4
@ printed when player enters invalid character (ie not a letter)
invalidmsg: .asciz "INVALID INPUT"
.align 4
@ printed upon starting the game
welcome: .asciz "Welcome to Hangman\nAuthor: Robyn Greer\nTo exit the game enter 0"
.align 4
@ printed along with dashes and guessed characters representing secret word
word: .asciz "Word: "
.align 4
@ dashes for printing the hidden word
dash: .asciz "_"
.align 4
@ where the player's correct guesses are stored
allinput: .asciz " , , , , , , , , , , , , , , , , , ,"
.align 4
@ character to be printed in printchar function stored here
chartoprint: .asciz " "
.align 4
@ printed along with any incorrectly guessed letters
missesmsg: .asciz "Misses: "
.align 4
@ all the potential gallows
g6hangman: .asciz  "\n\n   |-----|\n   |     |\n         |\n         |\n         |\n         |\n         |\n         |\n___________\n"
g5hangman: .asciz  "\n\n   |-----|\n   |     |\n   O     |\n         |\n         |\n         |\n         |\n         |\n___________\n"
g4hangman: .asciz  "\n\n   |-----|\n   |     |\n   O     |\n   |     |\n   |     |\n         |\n         |\n         |\n___________\n"
g3hangman: .asciz  "\n\n   |-----|\n   |     |\n   O     |\n  /|     |\n   |     |\n         |\n         |\n         |\n___________\n"
g2hangman: .asciz  "\n\n   |-----|\n   |     |\n   O     |\n  /|\\    |\n   |     |\n         |\n         |\n         |\n___________\n"
g1hangman: .asciz  "\n\n   |-----|\n   |     |\n   O     |\n  /|\\    |\n   |     |\n  /      |\n         |\n         |\n___________\n"
g0hangman: .asciz  "\n\n   |-----|\n   |     |\n   O     |\n  /|\\    |\n   |     |\n  / \\    |\n         |\n         |\n___________\n"
.align 4
@ message printed upon exiting the program
endmsg: .asciz "Ending program"
.align 4
@ current gallows format stored here
hangman: .asciz " "
.align 4
@ a new line
newline: .asciz "\n"
